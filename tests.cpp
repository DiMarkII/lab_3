#include "CppUnitTest.h"
#include "main.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
    TEST_CLASS(tests){
    public :
        TEST_METHOD(TestAlgorithm)
        {
            vector<string> names = {"1", "2", "3"};
            vector<vector<edge>> adj_list = {{{1, 10}, {2, 20}}, {{0, 5}}, {{0, 10}}};
            int ind_from = 1;
            int ind_to = 2;
            vector<path_info> paths = get_paths(adj_list, ind_from);
            Assert::IsTrue(get_string_path(paths, names, ind_from, ind_to) == "2 -> 1 -> 3");
            Assert::IsTrue(paths[ind_to].total_cost == 25);
        }
    };
}