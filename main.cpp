#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

struct edge
{
    int vert_ind;
    int cost;
};
struct path_info
{
    int total_cost;
    int from;
};

void get_data(vector<string> &first_cities, vector<string> &second_cities, vector<int> &first_ind, vector<int> &second_ind);
vector<string> get_cities(vector<string> &first_cities, vector<string> &second_cities);
vector<vector<edge>> get_adj_list(vector<string> &cities, vector<int> &first_ind, vector<int> &second_ind, vector<string> &first_cities, vector<string> &second_cities);
vector<path_info> get_paths(vector<vector<edge>> &adj_list, int ind);
string get_string_path(vector<path_info> &paths, vector<string> &cities, int ind1, int ind2);

int main()
{
    vector<string> first_cities, second_cities;
    vector<int> first_ind, second_ind;
    get_data(first_cities, second_cities, first_ind, second_ind);
    vector<string> cities = get_cities(first_cities, second_cities);
    vector<vector<edge>> adj_list = get_adj_list(cities, first_ind, second_ind, first_cities, second_cities);
    string city_from, city_to;
    std::cout << "From what city: ";
    getline(cin, city_from);
    std::cout << "To what city: ";
    getline(cin, city_to);
    int ind_from, ind_to;
    ind_from = find(cities.begin(), cities.end(), city_from) - cities.begin();
    ind_to = find(cities.begin(), cities.end(), city_to) - cities.begin();
    vector<path_info> paths = get_paths(adj_list, ind_from);
    std::cout << "Path: " << get_string_path(paths, cities, ind_from, ind_to) << '\n' << "Cost: " << paths[ind_to].total_cost << '\n';
    return 0;
}

void get_data(vector<string> &first_cities, vector<string> &second_cities, vector<int> &first_ind, vector<int> &second_ind)
{
    ifstream fin("input.txt");
    string line;
    while(fin)
    {
        getline(fin, line);
        if (!fin)
            break;
        std::string temp;
        int index_1 = 0, index_2;
        index_2 = line.find_first_of(";", index_1);
        temp = line.substr(index_1, index_2 - index_1);
        first_cities.push_back(temp);
        index_1 = index_2 + 1;
        index_2 = line.find_first_of(";", index_1);
        temp = line.substr(index_1, index_2 - index_1);
        second_cities.push_back(temp);
        index_1 = index_2 + 1;
        index_2 = line.find_first_of(";", index_1);
        temp = line.substr(index_1, index_2 - index_1);
        if (temp == "N/A")
            first_ind.push_back(-1);
        else
            first_ind.push_back(stoi(temp));
        index_1 = index_2 + 1;
        temp = line.substr(index_1, line.size() - index_1);
        if (temp == "N/A")
            second_ind.push_back(-1);
        else
            second_ind.push_back(stoi(temp));
    }
}

vector<string> get_cities(vector<string> &first_cities, vector<string> &second_cities)
{
    vector<string> result;
    for(int i = 0; i < first_cities.size(); ++i)
        if (find(result.begin(), result.end(), first_cities[i]) == result.end())
            result.push_back(first_cities[i]);
    for(int i = 0; i < second_cities.size(); ++i)
        if (find(result.begin(), result.end(), second_cities[i]) == result.end())
            result.push_back(second_cities[i]);
    return result;
}

vector<vector<edge>> get_adj_list(vector<string> &cities, vector<int> &first_ind, vector<int> &second_ind, vector<string> &first_cities, vector<string> &second_cities)
{
    vector<vector<edge>> result(cities.size());
    for(int i = 0; i < first_ind.size(); ++i)
    {
        int ind_1 = find(cities.begin(), cities.end(), first_cities[i]) - cities.begin();
        int ind_2 = find(cities.begin(), cities.end(), second_cities[i]) - cities.begin();
        result[ind_1].push_back({ind_2, first_ind[i]});
        result[ind_2].push_back({ind_1, second_ind[i]});
    }
    return result;
}

vector<path_info> get_paths(vector<vector<edge>> &adj_list, int ind)
{
    int inf = 10000000;
    vector<path_info> result(adj_list.size(), {inf, -1});
    result[ind] = {0, -1};
    vector<int> visited, need_to_visit;
    for(int i = 0; i < adj_list[ind].size(); ++i)
        need_to_visit.push_back(adj_list[ind][i].vert_ind);
    need_to_visit.push_back(ind);
    while(need_to_visit.size())
    {
        int new_ind = need_to_visit[need_to_visit.size() - 1];
        need_to_visit.pop_back();
        for(int i = 0; i < adj_list[new_ind].size(); ++i)
        {
            if (result[new_ind].total_cost + adj_list[new_ind][i].cost < result[adj_list[new_ind][i].vert_ind].total_cost)
            {
                result[adj_list[new_ind][i].vert_ind].total_cost = result[new_ind].total_cost + adj_list[new_ind][i].cost;
                result[adj_list[new_ind][i].vert_ind].from = new_ind;
            }
            if (find(visited.begin(), visited.end(), adj_list[new_ind][i].vert_ind) == visited.end())
                need_to_visit.push_back(adj_list[new_ind][i].vert_ind);
        }
        visited.push_back(new_ind);
    }
    return result;
}

string get_string_path(vector<path_info> &paths, vector<string> &cities, int ind1, int ind2)
{
    vector<int> ind_path;
    int ind_from = paths[ind2].from;
    ind_path.push_back(ind2);
    while(ind_from != -1)
    {
        ind_path.push_back(ind_from);
        ind_from = paths[ind_from].from;
    }
    reverse(ind_path.begin(), ind_path.end());
    string result;
    for(int i = 0; i < ind_path.size(); ++i)
    {
        result += cities[ind_path[i]];
        if (i < ind_path.size() - 1)
            result += " -> ";
    }
    return result;
}